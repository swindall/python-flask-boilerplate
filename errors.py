from flask import render_template
from dataclasses import dataclass


@dataclass
class ErrorDescription:
    code: int
    title: str
    description: str


def render_error(data):
    return render_template('errors/todo.html', data=data), data.code


def register_error_handler(app):
    @app.errorhandler(401)
    def unauthorized_error(error):
        return render_error(ErrorDescription(401, 'Unauthorized', 'Access is denied due to invalid credentials'))

    @app.errorhandler(403)
    def forbidden_error(error):
        return render_error(ErrorDescription(403, 'Forbidden', 'I\'m sorry Dave, I\'m afraid I can\'t let you do that.'))

    @app.errorhandler(404)
    def not_found_error(error):
        return render_error(ErrorDescription(404, 'Not Found', 'You\'re in the wrong place.'))

    @app.errorhandler(500)
    def internal_error(error):
        return render_error(ErrorDescription(500, 'Internal Fuck Up', 'Something unexpected went wrong...'))
