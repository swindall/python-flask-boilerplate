FROM python:3.7-alpine
ENV APPLICATION_PORT 80
ENV APPLICATION_DEBUG False
COPY requirements.txt /
RUN pip install -r /requirements.txt
COPY . /app
WORKDIR /app
CMD ["python", "app.py"]
EXPOSE 80