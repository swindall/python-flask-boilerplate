from flask import render_template


def register_example_endpoints(app):

    @app.route('/')
    def home():
        return render_template('pages/todo.html')

    @app.route('/log_some_stuff')
    def about():
        app.logger.error('error')
        app.logger.warn('warn')
        app.logger.info('info')
        app.logger.debug('debug')
        return render_template('pages/todo.html')