import os

# Default config:

HOST = '0.0.0.0'
PORT = 5000

DEBUG = True

COMPRESSOR_OUTPUT_DIR = './static/dist'
COMPRESSOR_STATIC_PREFIX = '/static'

GOOGLE_ID = 'TODO.apps.googleusercontent.com'
GOOGLE_SECRET = 'TODO'
GOOGLE_AUTHORISED_URI = '/login/authorised'
GOOGLE_OAUTH_DOMAIN = 'console.com.au'

SERVICE_NAME = os.environ.get('SERVICE_NAME', 'Flask Application')
SERVICE_CHECK_HTTP = os.environ.get('SERVICE_CHECK_HTTP', '/health')


# Load overrides from the environment variables:

prefix = 'APPLICATION_'
for key, value in os.environ.items():
    if key.startswith(prefix):
        key = key[len(prefix):]
        if value.lower() in ('true', 'false'):
            value = True if value.lower() == 'true' else False
        elif '.' in value:
            try:
                value = float(value)
            except ValueError:
                pass
        else:
            try:
                value = int(value)
            except ValueError:
                pass
        vars()[key] = value


# Conditional config:

COMPRESSOR_DEBUG = vars().get('COMPRESSOR_DEBUG', DEBUG)