import sys
import signal
import config
from logging.config import fileConfig as loadLoggingConfig
from flask import Flask
from jac.contrib.flask import JAC
from errors import register_error_handler
from auth import register_auth_endpoints
from example import register_example_endpoints

loadLoggingConfig('logging.cfg')

app = Flask(config.SERVICE_NAME)
app.config.from_object(config)

JAC(app)
app.jinja_env.line_statement_prefix = '%'

register_error_handler(app)
register_auth_endpoints(app)
register_example_endpoints(app)


@app.route(config.SERVICE_CHECK_HTTP, methods=['GET'])
def health():
    return 'Perfectly imperfect...', 200


@app.teardown_appcontext
def shutdown_session(exception=None):
    # Close any connections (DB, etc)
    pass


def handle_sigterm(signum, frame):
    app.teardown_appcontext()
    sys.exit(0)


def run():
    signal.signal(signal.SIGTERM, handle_sigterm)
    if config.DEBUG:
        # Run on a development server - with live reload
        app.run(host=config.HOST, port=config.PORT)
    else:
        # Run with a production WSGI server
        from waitress import serve
        from paste.translogger import TransLogger
        serve(
            TransLogger(app, setup_console_handler=False),
            listen='%s:%s' % (config.HOST, config.PORT)
        )


if __name__ == '__main__':
    run()
