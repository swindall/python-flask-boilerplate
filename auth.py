from flask import request, redirect, url_for, jsonify, abort, session
from flask_oauthlib.client import OAuth
from urllib import parse as urllib
from functools import wraps
import config


# Function wrapper for endpoints that require authentication
def requires_authorisation():
    def decorator(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            if session.get('google_token') is None:
                state = urllib.urlparse(request.url)._replace(scheme='', netloc='').geturl()
                return redirect(url_for('login', next=state))
            domain = session.get('user').get('hd')
            if domain == config.GOOGLE_OAUTH_DOMAIN:
                return f(*args, **kwargs)
            abort(403)
        return decorated
    return decorator


# Register authentication endpoints backed by google
def register_auth_endpoints(wsgi):
    oauth = OAuth(wsgi)
    google = oauth.remote_app(
        'google',
        consumer_key=config.GOOGLE_ID,
        consumer_secret=config.GOOGLE_SECRET,
        request_token_params={
            'scope': 'email'
        },
        base_url='https://www.googleapis.com/oauth2/v1/',
        request_token_url=None,
        access_token_method='POST',
        access_token_url='https://accounts.google.com/o/oauth2/token',
        authorize_url='https://accounts.google.com/o/oauth2/auth',
    )

    @wsgi.route('/user/data/', methods=['GET'])
    def user_data():
        # Test function to see available data
        if 'google_token' in session:
            return jsonify({'data': google.get('userinfo')})
        abort(403)

    @wsgi.route('/login/', methods=['GET'])
    def login():
        state = request.args.get('next', '/')
        return google.authorize(callback=url_for('authorised', _external=True), state=state)

    @wsgi.route(config.GOOGLE_AUTHORISED_URI)
    def authorised():
        resp = google.authorized_response()
        if resp is None:
            # Details:
            # request.args['error_reason']
            # request.args['error_description']
            abort(401)
        session['google_token'] = (resp['access_token'], '')
        user = google.get('userinfo').data
        session['user'] = user
        state = request.args.get('state', '/')
        return redirect(state)

    @wsgi.route('/logout/', methods=['GET'])
    def logout():
        session.pop('google_token', None)
        return redirect('/')

    @google.tokengetter
    def get_google_oauth_token():
        return session.get('google_token')